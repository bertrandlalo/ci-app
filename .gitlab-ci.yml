variables:
  PYTHON3_IMAGE: python:alpine
  CONTAINER_COMMIT_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  CONTAINER_LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  REDIS_HOST: redis 

.on_commit_to_branch_or_mr_to_master:
  rules:
    # $CI_COMMIT_BRANCH: The commit branch name.
    #  Present in branch pipelines, including pipelines for the default branch.
    #  Not present in merge request pipelines or tag pipelines.
    - if: '$CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: always

.on_master: 
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always

.before_script: 
    before_script:
    - apk add --no-cache gcc musl-dev

stages:
  - syntax
  - sast
  - unit_tests
  - build
  - integration_tests
  - release

PyCodeStyle:
  extends: .on_commit_to_branch_or_mr_to_master
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
    - pip install pycodestyle     
    - pycodestyle app.py

Pylint:
  extends: 
    - .on_commit_to_branch_or_mr_to_master
    - .before_script
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
    - pip install -r requirements.txt
    - pylint app.py

Security:
  extends: 
    - .on_commit_to_branch_or_mr_to_master
    - .before_script
  image: $PYTHON3_IMAGE
  stage: sast
  script:
    - pip install -r requirements.txt
    - bandit app.py

Unit Tests: 
  extends: 
    - .on_commit_to_branch_or_mr_to_master
    - .before_script
  image: $PYTHON3_IMAGE
  stage: unit_tests
  script: 
    - pip install -r requirements.txt
   # - python -m pytest --junit-xml=pytest-report.xml --cov=./
    - python3 -m pytest --junit-xml=pytest-report.xml --cov=./ --cov-report=html --cov-report=term
  artifacts:
    paths:
      - htmlcov
    reports:
      junit: pytest-report.xml
            
Build Image:
  extends: .on_master
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - test -z "$DOCKER_CONFIG" && export DOCKER_CONFIG=/kaniko/.docker
    - |
      cat > "${DOCKER_CONFIG}/config.json" <<EOF
      { "auths": { "${CI_REGISTRY}": { "username": "gitlab-ci-token", "password": "${CI_JOB_TOKEN}" } } }
      EOF
    - >-
        /kaniko/executor
        --context "${CI_PROJECT_DIR}"
        --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
        --build-arg APP_VERSION="${CI_COMMIT_SHORT_SHA}"
        --destination "${CONTAINER_COMMIT_IMAGE}"
        --destination "${CONTAINER_LATEST_IMAGE}"

Integ tests:
  extends: .on_master
  image:
    name: dduportal/bats:0.4.0
    entrypoint: [""]
  stage: integration_tests
  services:
  - name: $CONTAINER_COMMIT_IMAGE
    alias: my-app
  script:
  - $CI_PROJECT_DIR/gitlab-ci-scripts/test_bats.sh


Tag Image Docker for Release:
  stage: release
  image:
    name: solsson/crane@sha256:58647d756b9008f312827227d0344ca2a7439c99222668c95e93d99dcc94d9ac
    entrypoint: [""]
  script:
    - test -z "$DOCKER_CONFIG" && export DOCKER_CONFIG=/
    - |
      cat > "${DOCKER_CONFIG}/config.json" <<EOF
      { "auths": { "${CI_REGISTRY}": { "username": "gitlab-ci-token", "password": "${CI_JOB_TOKEN}" } } }
      EOF
    - crane cp "${CONTAINER_COMMIT_IMAGE}" "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
  rules:
    - if: '$CI_COMMIT_TAG'
      when: always